const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

const bodyParser = require('body-parser');
const { response } = require('express');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

const databse = 'realTimeMessaging';
const uri = 'mongodb+srv://faruk:327855@cluster0.cv7la.mongodb.net/' + databse + '?retryWrites=true&w=majority';
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

io.on('connection', (socket) => {
    console.log("Baglantı ok ", socket.client.id)
    socket.on("ready", () => {
        io.to(socket.id).emit("clientId", socket.id);
        getAllMessage(socket.id);
    });
    socket.on("send message", (message) => {
        insertData(message);
    });
    socket.on('disconnect', () => {
        console.log("disconnect");
    });
});

client.connect(err => {
    const collection = client.db(databse).collection("messages");
    const changeStream = collection.watch();
    changeStream.on('change', next => {
        io.emit("listen message", next.fullDocument);
    });
});

function insertData(message) {
    client.connect(function (err) {
        assert.equal(null, err);
        const db = client.db(databse);
        insertDocuments(db, message, function () {
            client.close();
        });
    });
}

insertDocuments = function (db, message, callback) {
    // Get the documents collection
    const collection = db.collection('messages');
    // Insert some documents
    collection.insertMany(
        message
        , function (err, result) {
            assert.equal(err, null);
            callback(result);
        });
}

function getAllMessage(socketId) {
    client.connect(function (err) {
        assert.equal(null, err);
        try {
            const db = client.db(databse);
            db.collection("messages").find({}).sort({ createdAt: -1 }).toArray(function (err, result) {
                if (err) throw err;
                io.to(socketId).emit("listen message", result);
                client.close();
            });

        } catch (err) {
            console.log(err)
        }

    });
}

async function connectDb() {
    return await client.connect()
        .catch(err => { console.log(err); });
}

async function getUserFromEmail(email) {
    const connection = await connectDb();
    if (!connection) {
        return;
    }
    try {
        const db = connection.db(databse);
        let collection = db.collection('users');
        let query = { email: email }
        let res = await collection.findOne(query);
        return res;
    } catch (err) {
        console.log(err);
    } finally {

        client.close();
    }
}

async function getLastIdFromCollectionName(collectionName) {
    const connection = await connectDb();
    if (!connection) {
        return;
    }
    try {
        const db = connection.db(databse);
        let collection = db.collection('lastIds');
        let query = { collectionName: collectionName }
        let res = await collection.findOne(query);
        return res;
    } catch (err) {
        console.log(err);
    } finally {

        client.close();
    }
}

async function setLastId(collectionName, newValue) {
    const connection = await connectDb();
    if (!connection) {
        return;
    }
    try {
        const db = connection.db(databse);
        let collection = db.collection('lastIds');
        return await collection.insertOne(
            {
                collectionName: collectionName,
                value: newValue
            });
    } catch (err) {
        console.log(err);
    } finally {
        client.close();
    }
}

async function updateLastId(collectionName, newValue) {
    const connection = await connectDb();
    if (!connection) {
        return;
    }
    try {
        const db = connection.db(databse);
        let collection = db.collection('lastIds');
        return collection.updateOne(
            { collectionName: collectionName },
            {
                $set: { value: newValue }
            });
    } catch (err) {
        console.log(err);
    } finally {
        client.close();
    }
}

async function signIn(_id, email, password) {
    if (!await getUserFromEmail(email)) {
        const connection = await connectDb();
        if (!connection) {
            return;
        }
        try {
            let lastId = await getLastIdFromCollectionName('users');
            if (!lastId) {
                lastId = await (await setLastId('users', 0)).ops[0];
            }
            const db = connection.db(databse);
            let collection = db.collection('users');
            return collection.insertOne(
                {
                    _id: lastId.value,
                    email: email,
                    password: password
                });
        } catch (err) {
            console.log(err);
        } finally {
            client.close();
        }
    }
    else {
        return "kullanıcı zaten mevcut";
    }
}

app.get('/', function (req, res) {
    getAllMessage();
    res.send("hello world");
});

app.post('/signIn', function (req, res) {
    const body = req.body;
    signIn(body._id, body.email, body.password).then(response => {
        updateLastId('users', response.insertedId + 1)
        res.send({
            result: response.ops ? response.ops[0] : response
        });
    }).catch(err => {
        res.send({
            result: 'error'
        });
    });

});


server.listen(3001);